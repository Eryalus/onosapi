package org.adrian.onosapi.engine;

public class ValidToken {
    private boolean token;

    public ValidToken(boolean defaultValue) {
        token = defaultValue;
    }

    public boolean isToken() {
        return token;
    }

    public void setToken(boolean token) {
        this.token = token;
    }

}
