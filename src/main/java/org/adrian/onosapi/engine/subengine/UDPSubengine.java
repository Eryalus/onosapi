package org.adrian.onosapi.engine.subengine;

import java.util.regex.Pattern;

import org.adrian.onosapi.engine.ValidToken;
import org.adrian.onosapi.entity.FwFilter;
import org.adrian.onosapi.utils.L4Ports;
import org.onlab.packet.IPv4;
import org.onlab.packet.TpPort;
import org.onosproject.net.flow.TrafficSelector.Builder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UDPSubengine extends L4Subengine implements Subengine {
    private static UDPSubengine instance = null;

    private UDPSubengine() {
        super();
    }

    public static UDPSubengine getInstance() {
        if (instance == null)
            instance = new UDPSubengine();
        return instance;
    }

    @Override
    protected void populateBuilders(Builder outgoingBuilder, Builder incomingBuilder, TpPort packetSrcPort,
            TpPort packetDstPort, ValidToken incomingBuilderApply, boolean allowReturn) {
        outgoingBuilder.matchIPProtocol(IPv4.PROTOCOL_UDP);
        if (allowReturn) {
            incomingBuilderApply.setToken(true);
            incomingBuilder.matchIPProtocol(IPv4.PROTOCOL_UDP);
            incomingBuilder.matchUdpSrc(packetDstPort);
            incomingBuilder.matchUdpDst(packetSrcPort);
        }
        outgoingBuilder.matchUdpSrc(packetSrcPort);
        outgoingBuilder.matchUdpDst(packetDstPort);
    }

}
