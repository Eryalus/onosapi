package org.adrian.onosapi.engine.subengine;

import java.util.regex.Pattern;

import org.adrian.onosapi.engine.ValidToken;
import org.adrian.onosapi.entity.FwFilter;
import org.adrian.onosapi.utils.L4Ports;
import org.onlab.packet.TpPort;
import org.onosproject.net.flow.TrafficSelector.Builder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class L4Subengine implements Subengine {
    private final Logger log = LoggerFactory.getLogger(getClass());

    private static final String VALUE_PATTERN = "^(([0-9]+(-[0-9]+)?[*]?),?)+$";
    private static final String SINGLE_FILTER_VALUE_PATTERN = "^[0-9]+[-][0-9]+$";

    protected final Pattern valuePattern;
    protected final Pattern singleFilterValuePattern;

    protected L4Subengine() {
        valuePattern = Pattern.compile(VALUE_PATTERN);
        singleFilterValuePattern = Pattern.compile(SINGLE_FILTER_VALUE_PATTERN);
    }

    protected boolean validPortBasedOnFilter(String filter, int port) {
        boolean valid = false;
        int from, to;
        try {
            if (singleFilterValuePattern.matcher(filter).find()) {
                String[] nums = filter.split("-");
                from = Integer.parseInt(nums[0]);
                to = Integer.parseInt(nums[1]);
            } else {
                from = Integer.parseInt(filter);
                to = Integer.parseInt(filter);
            }
            valid = port >= from && port <= to;
        } catch (NumberFormatException ex) {
            log.error(ex.getMessage(), ex);
        }
        return valid;
    }

    public void processFilter(FwFilter filter, Builder outgoingBuilder, Builder incomingBuilder,
            L4Ports currentPacketPorts, ValidToken validToken, ValidToken incomingBuilderApply) {
        if (currentPacketPorts == null) {
            validToken.setToken(false);
            return;
        }
        String protocolValue = filter.getProtocolValue();
        TpPort packetSrcPort = TpPort.tpPort(currentPacketPorts.getSrcPort());
        TpPort packetDstPort = TpPort.tpPort(currentPacketPorts.getDstPort());
        if (valuePattern.matcher(protocolValue).find()) {
            if (protocolValue.endsWith(","))
                protocolValue = protocolValue.substring(0, protocolValue.length() - 1);
            String[] singleFilters = protocolValue.split(",");
            for (String singleFilter : singleFilters) {
                boolean allowReturn = singleFilter.endsWith("*");
                if (allowReturn) {
                    singleFilter = singleFilter.substring(0, singleFilter.length() - 1);
                    log.info("Allow return");
                }
                boolean valid = validPortBasedOnFilter(singleFilter,
                        filter.isOrigin() ? packetSrcPort.toInt() : packetDstPort.toInt());
                validToken.setToken(valid);
                if (valid) {
                    populateBuilders(outgoingBuilder, incomingBuilder, packetSrcPort, packetDstPort,
                            incomingBuilderApply, allowReturn);
                    break;
                }
            }
        }
    }

    protected void populateBuilders(Builder outgoingBuilder, Builder incomingBuilder, TpPort packetSrcPort,
            TpPort packetDstPort, ValidToken incomingBuilderApply, boolean allowReturn){};

}
