package org.adrian.onosapi.engine.subengine;

import java.util.regex.Pattern;

import org.adrian.onosapi.engine.ValidToken;
import org.adrian.onosapi.entity.FwFilter;
import org.adrian.onosapi.utils.L4Ports;
import org.onlab.packet.IPv4;
import org.onlab.packet.TpPort;
import org.onosproject.net.flow.TrafficSelector.Builder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ICMPSubengine extends L4Subengine implements Subengine {
    private static ICMPSubengine instance = null;

    private ICMPSubengine() {
        super();
    }

    public static ICMPSubengine getInstance() {
        if (instance == null)
            instance = new ICMPSubengine();
        return instance;
    }

    @Override
    public void processFilter(FwFilter filter, Builder outgoingBuilder, Builder incomingBuilder,
            L4Ports currentPacketPorts, ValidToken validToken, ValidToken incomingBuilderApply) {
        if (currentPacketPorts == null) {
            validToken.setToken(false);
            return;
        }
        String protocolValue = filter.getProtocolValue();
        if (valuePattern.matcher(protocolValue).find()) {
            if (protocolValue.endsWith(","))
                protocolValue = protocolValue.substring(0, protocolValue.length() - 1);
            String[] singleFilters = protocolValue.split(",");
            for (String singleFilter : singleFilters) {
                boolean valid = validPortBasedOnFilter(singleFilter, currentPacketPorts.getSrcPort());
                validToken.setToken(valid);
                if (valid) {
                    outgoingBuilder.matchIPProtocol(IPv4.PROTOCOL_ICMP);
                    outgoingBuilder.matchIcmpType((byte) currentPacketPorts.getSrcPort());
                    break;
                }
            }
        } else if (protocolValue.isEmpty()) {
            validToken.setToken(true);
            outgoingBuilder.matchIPProtocol(IPv4.PROTOCOL_ICMP);
        }
    }

}
