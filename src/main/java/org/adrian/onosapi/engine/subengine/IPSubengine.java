package org.adrian.onosapi.engine.subengine;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.adrian.onosapi.engine.ValidToken;
import org.adrian.onosapi.entity.FwFilter;
import org.adrian.onosapi.utils.IPUtils;
import org.onlab.packet.IpPrefix;
import org.onosproject.net.flow.TrafficSelector.Builder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IPSubengine implements Subengine {
    private static IPSubengine instance = null;
    private final Logger log = LoggerFactory.getLogger(getClass());

    private IPSubengine() {

    }

    public static IPSubengine getInstance() {
        if (instance == null)
            instance = new IPSubengine();
        return instance;
    }

    public void processFilter(FwFilter filter, Builder outgoingBuilder, Builder incomingBuilder, InetAddress src,
            InetAddress dst, ValidToken validToken, ValidToken incomingBuilderApply) {
        try {
            if (filter.isOrigin() && IPUtils.isIPinSubnets(src, filter.getProtocolValue())) {
                IpPrefix srcIpPrefix = IpPrefix.valueOf(src.getHostAddress() + "/32");
                outgoingBuilder.matchIPSrc(srcIpPrefix);
                incomingBuilder.matchIPDst(srcIpPrefix);
            } else if (IPUtils.isIPinSubnets(dst, filter.getProtocolValue())) {
                IpPrefix dstIpPrefix = IpPrefix.valueOf(dst.getHostAddress() + "/32");
                outgoingBuilder.matchIPDst(dstIpPrefix);
                incomingBuilder.matchIPSrc(dstIpPrefix);
            } else {
                validToken.setToken(false);
            }
        } catch (UnknownHostException ex) {
            log.error(ex.getMessage(), ex);
        }
    }

}
