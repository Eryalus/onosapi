package org.adrian.onosapi.engine.subengine;

import org.adrian.onosapi.engine.ValidToken;
import org.onosproject.net.flow.TrafficSelector.Builder;
import org.onlab.packet.IPv4;
import org.onlab.packet.TpPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TCPSubengine extends L4Subengine implements Subengine {
    private static TCPSubengine instance = null;
    private final Logger log = LoggerFactory.getLogger(getClass());

    private TCPSubengine() {
        super();
    }

    public static TCPSubengine getInstance() {
        if (instance == null)
            instance = new TCPSubengine();
        return instance;
    }

    @Override
    protected void populateBuilders(Builder outgoingBuilder, Builder incomingBuilder, TpPort packetSrcPort,
            TpPort packetDstPort, ValidToken incomingBuilderApply, boolean allowReturn) {
        outgoingBuilder.matchIPProtocol(IPv4.PROTOCOL_TCP);
        if (allowReturn) {
            incomingBuilderApply.setToken(true);
            incomingBuilder.matchIPProtocol(IPv4.PROTOCOL_TCP);
            incomingBuilder.matchTcpSrc(packetDstPort);
            incomingBuilder.matchTcpDst(packetSrcPort);
        }
        outgoingBuilder.matchTcpSrc(packetSrcPort);
        outgoingBuilder.matchTcpDst(packetDstPort);
    }

}
