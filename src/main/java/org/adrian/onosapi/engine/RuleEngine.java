package org.adrian.onosapi.engine;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.adrian.onosapi.container.RuleContainer;
import org.adrian.onosapi.engine.subengine.ICMPSubengine;
import org.adrian.onosapi.engine.subengine.IPSubengine;
import org.adrian.onosapi.engine.subengine.TCPSubengine;
import org.adrian.onosapi.engine.subengine.UDPSubengine;
import org.adrian.onosapi.entity.FwFilter;
import org.adrian.onosapi.entity.FwRule;
import org.adrian.onosapi.entity.FwRuleGroup;
import org.adrian.onosapi.utils.ICMPUtils;
import org.adrian.onosapi.utils.IPUtils;
import org.adrian.onosapi.utils.L4Ports;
import org.adrian.onosapi.utils.TCPUtils;
import org.adrian.onosapi.utils.UDPUtils;
import org.onlab.packet.Ethernet;
import org.onlab.packet.IP;
import org.onlab.packet.IPacket;
import org.onlab.packet.IPv4;
import org.onlab.packet.IpPrefix;
import org.onlab.packet.TpPort;
import org.onosproject.net.flow.DefaultTrafficSelector;
import org.onosproject.net.flow.TrafficSelector.Builder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RuleEngine {
    private static RuleEngine instance;
    private final Logger log = LoggerFactory.getLogger(getClass());
    /**
     * Special one. Used to mark if IP protocol matcher has been added
     */
    private static final Integer IP_PROTO = 0;
    private static final Integer TCP_PROTO = 1;
    private static final Integer UDP_PROTO = 2;
    private static final Integer ICMP_PROTO = 3;

    public static RuleEngine getInstance() {
        if (instance == null)
            instance = new RuleEngine();
        return instance;
    }

    private RuleEngine() {

    }

    public static class RuleSelector {
        private Builder selector;
        private Builder incomingSelector;
        private boolean drop = true;

        public Builder getSelector() {
            return selector;
        }

        public void setSelector(Builder selector) {
            this.selector = selector;
        }

        public boolean isDrop() {
            return drop;
        }

        public void setDrop(boolean drop) {
            this.drop = drop;
        }

        public Builder getIncomingSelector() {
            return incomingSelector;
        }

        public void setIncomingSelector(Builder incomingSelector) {
            this.incomingSelector = incomingSelector;
        }

        public RuleSelector(Builder selector, Builder incomingSelector, boolean drop) {
            this.selector = selector;
            this.incomingSelector = incomingSelector;
            this.drop = drop;
        }

    }

    public ArrayList<RuleSelector> calculateRulesForPacket(IPacket ipPacket) {
        ArrayList<RuleSelector> selectors = new ArrayList<>();
        if (ipPacket instanceof IPv4) {
            InetAddress[] ipAddressesFromPacket = IPUtils.getIPAddressesFromPacket((IPv4) ipPacket);
            if (ipAddressesFromPacket == null)
                return selectors;
            L4Ports tcpPorts = new TCPUtils().getPortsForPatcket(ipPacket);
            L4Ports udpPorts = new UDPUtils().getPortsForPatcket(ipPacket);
            L4Ports icmpTypeAsPorts = new ICMPUtils().getTypeAsPorts(ipPacket);
            InetAddress src = ipAddressesFromPacket[0];
            InetAddress dst = ipAddressesFromPacket[1];
            List<FwRuleGroup> groups = RuleContainer.getInstance().getRules();
            Collections.sort(groups);
            for (FwRuleGroup group : groups) {
                Set<FwRule> fwRules = group.getFwRules();
                ArrayList<FwRule> rules = new ArrayList<>();
                for (FwRule r : fwRules) {
                    rules.add(r);
                }
                Collections.sort(rules);
                for (FwRule rule : rules) {
                    Builder outgoingBuilder = DefaultTrafficSelector.builder();
                    Builder incomingBuilder = DefaultTrafficSelector.builder();
                    HashMap<Integer, Boolean> processedProtocols = new HashMap<>();
                    processedProtocols.put(IP_PROTO, false);
                    processedProtocols.put(TCP_PROTO, false);
                    processedProtocols.put(UDP_PROTO, false);
                    processedProtocols.put(ICMP_PROTO, false);
                    ValidToken validToken = new ValidToken(true);
                    ValidToken incomingBuilderToken = new ValidToken(false);
                    outgoingBuilder.matchEthType(Ethernet.TYPE_IPV4);
                    incomingBuilder.matchEthType(Ethernet.TYPE_IPV4);
                    for (FwFilter filter : rule.getFwFilters()) {
                        if (!validToken.isToken()) {
                            break;
                        }
                        if (filter.getProtocol().toLowerCase().equals("ipv4")) {
                            IPSubengine.getInstance().processFilter(filter, outgoingBuilder, incomingBuilder, src, dst,
                                    validToken, incomingBuilderToken);
                        } else if (filter.getProtocol().toLowerCase().equals("tcp")) {
                            TCPSubengine.getInstance().processFilter(filter, outgoingBuilder, incomingBuilder, tcpPorts,
                                    validToken, incomingBuilderToken);
                            processedProtocols.put(TCP_PROTO, true);
                        } else if (filter.getProtocol().toLowerCase().equals("udp")) {
                            UDPSubengine.getInstance().processFilter(filter, outgoingBuilder, incomingBuilder, udpPorts,
                                    validToken, incomingBuilderToken);
                            processedProtocols.put(UDP_PROTO, true);
                        } else if (filter.getProtocol().toLowerCase().equals("icmp")) {
                            ICMPSubengine.getInstance().processFilter(filter, outgoingBuilder, incomingBuilder, icmpTypeAsPorts,
                                    validToken, incomingBuilderToken);
                            processedProtocols.put(ICMP_PROTO, true);
                            log.info("ICMP"+icmpTypeAsPorts.getSrcPort());
                        }
                    }

                    // Fill non-processed protocols
                    if (!processedProtocols.get(IP_PROTO)) {
                        outgoingBuilder.matchIPProtocol(((IPv4) ipPacket).getProtocol());
                    }
                    if (!processedProtocols.get(TCP_PROTO) && tcpPorts != null) {
                        outgoingBuilder.matchIPProtocol(IPv4.PROTOCOL_TCP);
                        outgoingBuilder.matchTcpSrc(TpPort.tpPort(tcpPorts.getSrcPort()));
                        outgoingBuilder.matchTcpDst(TpPort.tpPort(tcpPorts.getDstPort()));
                    }
                    if (!processedProtocols.get(UDP_PROTO) && udpPorts != null) {
                        outgoingBuilder.matchIPProtocol(IPv4.PROTOCOL_UDP);
                        outgoingBuilder.matchUdpSrc(TpPort.tpPort(udpPorts.getSrcPort()));
                        outgoingBuilder.matchUdpDst(TpPort.tpPort(udpPorts.getDstPort()));
                    }
                    if (validToken.isToken()) {
                        selectors.add(new RuleSelector(outgoingBuilder,
                                incomingBuilderToken.isToken() ? incomingBuilder : null,
                                rule.getAction().equals("drop")));
                        break;
                    }
                }
            }
        }
        return selectors;
    }

}
