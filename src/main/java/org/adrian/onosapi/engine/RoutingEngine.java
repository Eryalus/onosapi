package org.adrian.onosapi.engine;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.adrian.onosapi.AppComponent;
import org.adrian.onosapi.container.TopologyContainer;
import org.adrian.onosapi.engine.RuleEngine.RuleSelector;
import org.adrian.onosapi.utils.IPUtils;
import org.onlab.packet.Ethernet;
import org.onlab.packet.IPv4;
import org.onlab.packet.IpAddress;
import org.onlab.packet.IpPrefix;
import org.onlab.packet.MacAddress;
import org.onosproject.net.DeviceId;
import org.onosproject.net.ElementId;
import org.onosproject.net.Host;
import org.onosproject.net.HostId;
import org.onosproject.net.HostLocation;
import org.onosproject.net.Link;
import org.onosproject.net.Path;
import org.onosproject.net.flow.DefaultFlowRule;
import org.onosproject.net.flow.DefaultTrafficSelector;
import org.onosproject.net.flow.DefaultTrafficTreatment;
import org.onosproject.net.flow.FlowRule;
import org.onosproject.net.flow.FlowRuleService;
import org.onosproject.net.flow.TrafficSelector.Builder;
import org.onosproject.net.host.HostService;
import org.onosproject.net.packet.InboundPacket;
import org.onosproject.net.topology.PathService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoutingEngine {

    private final Logger log = LoggerFactory.getLogger(getClass());
    private static RoutingEngine instance = null;
    private final PathService pathService;
    private final HostService hostService;
    private final FlowRuleService flowRuleService;

    public static RoutingEngine getInstance() {
        return instance;
    }

    public static RoutingEngine initializeInstance(PathService pathService, HostService hostService,
            FlowRuleService flowRuleService) {
        if (instance == null)
            instance = new RoutingEngine(pathService, hostService, flowRuleService);
        return (RoutingEngine) instance;
    }

    public void getPathForPacket(Ethernet rawPacket) {
        if (rawPacket.getPayload() instanceof IPv4) {
            IPv4 ipPacket = (IPv4) rawPacket.getPayload();
            InetAddress[] ipAddressesFromPacket = IPUtils.getIPAddressesFromPacket(ipPacket);
            if (ipAddressesFromPacket == null)
                return;
            InetAddress src = ipAddressesFromPacket[0];
            InetAddress dst = ipAddressesFromPacket[1];
            Host srcHost = null, dstHost = null;
            for (Host host : hostService.getHosts()) {
                for (IpAddress ip : host.ipAddresses()) {
                    String ipS = ip.toInetAddress().getHostAddress();
                    if (ipS.equals(src.getHostAddress())) {
                        srcHost = host;
                    } else if (ipS.equals(dst.getHostAddress())) {
                        dstHost = host;
                    }
                }
            }
            DeviceId directConnectionElementId = null;
            for (HostLocation srcLocation : srcHost.locations()) {
                for (HostLocation dstLocation : dstHost.locations()) {
                    if (srcLocation.deviceId().equals(dstLocation.deviceId())) {
                        // directly connected
                        directConnectionElementId = srcLocation.deviceId();
                    }
                }
            }
            log.info("Directly connected: " + (directConnectionElementId != null));
            ArrayList<RuleSelector> calculateRulesForHost = RuleEngine.getInstance().calculateRulesForPacket(ipPacket);
            log.info("Rules size: " + calculateRulesForHost.size());
            if (directConnectionElementId == null) {
                Set<Path> paths = this.pathService.getPaths(srcHost.location().elementId(),
                        dstHost.location().deviceId());
                log.info("Path size: " + paths.size());
                for (Path p : paths) {
                    int size = p.links().size();
                    int selectorCounter = 0;
                    for (RuleSelector selector : calculateRulesForHost) {
                        log.info("Selector apply incoming: " + (selector.getIncomingSelector() != null));
                        int counter = 0;
                        // outgoing packet
                        for (Link l : p.links()) {
                            DeviceId device = l.src().deviceId();
                            FlowRule fr;
                            org.onosproject.net.flow.TrafficTreatment.Builder treatment = null;
                            if (selector.isDrop())
                                treatment = DefaultTrafficTreatment.builder().drop();
                            else
                                treatment = DefaultTrafficTreatment.builder().setOutput(l.src().port());
                            fr = DefaultFlowRule.builder().forTable(0).forDevice(device)
                                    .withSelector(selector.getSelector().build()).withTreatment(treatment.build())
                                    .withPriority(30050 + (calculateRulesForHost.size() - selectorCounter))
                                    .makeTemporary(10).fromApp(AppComponent.appId).build();

                            flowRuleService.applyFlowRules(fr);
                            if (counter++ == size - 1) { // AKA last
                                device = l.dst().deviceId();
                                if (selector.isDrop())
                                    treatment = DefaultTrafficTreatment.builder().drop();
                                else
                                    treatment = DefaultTrafficTreatment.builder().setOutput(dstHost.location().port());
                                fr = DefaultFlowRule.builder().forTable(0).forDevice(device)
                                        .withSelector(selector.getSelector().build()).withTreatment(treatment.build())
                                        .withPriority(30050 + (calculateRulesForHost.size() - selectorCounter))
                                        .makeTemporary(10).fromApp(AppComponent.appId).build();

                                flowRuleService.applyFlowRules(fr);
                            }
                        }
                        Builder incomingSelector = selector.getIncomingSelector();
                        if (incomingSelector != null) {
                            log.info("Creating incomingPacket, drop: " + selector.isDrop());
                            // incoming packets (reverse path, dst->src)
                            counter = 0;
                            for (Link l : p.links()) {
                                DeviceId device = l.dst().deviceId();
                                FlowRule fr;
                                org.onosproject.net.flow.TrafficTreatment.Builder treatment = null;
                                if (selector.isDrop())
                                    treatment = DefaultTrafficTreatment.builder().drop();
                                else
                                    treatment = DefaultTrafficTreatment.builder().setOutput(l.dst().port());
                                fr = DefaultFlowRule.builder().forTable(0).forDevice(device)
                                        .withSelector(incomingSelector.build()).withTreatment(treatment.build())
                                        .withPriority(30050 + (calculateRulesForHost.size() - selectorCounter))
                                        .makeTemporary(10).fromApp(AppComponent.appId).build();

                                flowRuleService.applyFlowRules(fr);
                                if (counter++ == 0) { // AKA first
                                    device = l.src().deviceId();
                                    if (selector.isDrop())
                                        treatment = DefaultTrafficTreatment.builder().drop();
                                    else
                                        treatment = DefaultTrafficTreatment.builder()
                                                .setOutput(srcHost.location().port());
                                    fr = DefaultFlowRule.builder().forTable(0).forDevice(device)
                                            .withSelector(incomingSelector.build()).withTreatment(treatment.build())
                                            .withPriority(30050 + (calculateRulesForHost.size() - selectorCounter))
                                            .makeTemporary(10).fromApp(AppComponent.appId).build();

                                    flowRuleService.applyFlowRules(fr);
                                }
                            }
                        }

                        selectorCounter++;
                    }
                    break;
                }
            } else {
                // Directly connected
                int selectorCounter = 0;
                FlowRule fr;
                org.onosproject.net.flow.TrafficTreatment.Builder treatment = null;
                for (RuleSelector selector : calculateRulesForHost) {
                    // outgoing packet
                    if (selector.isDrop())
                        treatment = DefaultTrafficTreatment.builder().drop();
                    else
                        treatment = DefaultTrafficTreatment.builder().setOutput(dstHost.location().port());
                    fr = DefaultFlowRule.builder().forTable(0).forDevice(directConnectionElementId)
                            .withSelector(selector.getSelector().build()).withTreatment(treatment.build())
                            .withPriority(30050 + (calculateRulesForHost.size() - selectorCounter)).makeTemporary(10)
                            .fromApp(AppComponent.appId).build();

                    flowRuleService.applyFlowRules(fr);
                    // incoming packet
                    if (selector.getIncomingSelector() != null) {
                        if (selector.isDrop())
                            treatment = DefaultTrafficTreatment.builder().drop();
                        else
                            treatment = DefaultTrafficTreatment.builder().setOutput(srcHost.location().port());
                        fr = DefaultFlowRule.builder().forTable(0).forDevice(directConnectionElementId)
                                .withSelector(selector.getIncomingSelector().build()).withTreatment(treatment.build())
                                .withPriority(30050 + (calculateRulesForHost.size() - selectorCounter))
                                .makeTemporary(10).fromApp(AppComponent.appId).build();

                        flowRuleService.applyFlowRules(fr);
                    }
                    selectorCounter++;
                }
            }
        }
    }

    public RoutingEngine(PathService pathService, HostService hostService, FlowRuleService flowRuleService) {
        this.pathService = pathService;
        this.hostService = hostService;
        this.flowRuleService = flowRuleService;
    }

}
