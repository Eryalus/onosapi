package org.adrian.onosapi.processor;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.concurrent.Semaphore;

import org.adrian.onosapi.container.RuleContainer;
import org.adrian.onosapi.engine.RoutingEngine;
import org.adrian.onosapi.processor.utils.PacketCounter;
import org.onlab.packet.Ethernet;
import org.onlab.packet.IPacket;
import org.onlab.packet.IPv4;
import org.onosproject.net.PortNumber;
import org.onosproject.net.flow.DefaultTrafficTreatment;
import org.onosproject.net.flow.TrafficTreatment;
import org.onosproject.net.packet.DefaultOutboundPacket;
import org.onosproject.net.packet.InboundPacket;
import org.onosproject.net.packet.OutboundPacket;
import org.onosproject.net.packet.PacketContext;
import org.onosproject.net.packet.PacketProcessor;
import org.onosproject.net.packet.PacketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReactiveRulesProcessor implements PacketProcessor {

    private final Logger log = LoggerFactory.getLogger(getClass());
    private final PacketService packetService;
    private static final HashMap<String, PacketCounter> packetCounters = new HashMap<>();
    private static final long TTL = 1000l;
    private static final long CLEAN_TTL = 10000l; // every 10s
    private static long lastClean = 0;
    private static Semaphore packetCounteSemaphore = new Semaphore(1);

    public ReactiveRulesProcessor(PacketService packetService) {
        this.packetService = packetService;
    }

    @Override
    public void process(PacketContext context) {
        if (context.isHandled()) { // ignore already handled packets
            return;
        }
        if (lastClean < (System.currentTimeMillis() - CLEAN_TTL)) {
            try {
                packetCounteSemaphore.acquire();
                cleanCounters();
                lastClean = System.currentTimeMillis();
                packetCounteSemaphore.release();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                log.error(e.getMessage(), e);
            }
        }
        log.info("paquetito");
        InboundPacket inPacket = context.inPacket();
        Ethernet ethPacket = (Ethernet) inPacket.parsed().clone();
        IPacket packet = ethPacket.getPayload();
        if (packet instanceof IPv4) {
            RoutingEngine.getInstance().getPathForPacket(ethPacket);
        }
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-1");
            digest.update(inPacket.unparsed().array());
            byte[] sha = digest.digest();
            boolean send = false;
            String shaS = Base64.getEncoder().encodeToString(sha);
            PacketCounter counter;
            packetCounteSemaphore.acquire();
            if ((counter = packetCounters.get(shaS)) != null) {
                send = false;
                if (counter.getCount() < 5 && counter.getTimestamp() > (System.currentTimeMillis() - TTL)) {
                    send = true;
                    counter.addCount();
                } else {
                    packetCounters.remove(shaS);
                }
            } else {
                send = true;
                packetCounters.put(shaS, new PacketCounter(System.currentTimeMillis()));
            }
            packetCounteSemaphore.release();

            if (send) {
                TrafficTreatment sendTreatment = DefaultTrafficTreatment.builder().setOutput(PortNumber.TABLE).build();
                OutboundPacket outPacket = new DefaultOutboundPacket(inPacket.receivedFrom().deviceId(), sendTreatment,
                        inPacket.unparsed());
                packetService.emit(outPacket);
            }
        } catch (NoSuchAlgorithmException | InterruptedException e) {
            // TODO Auto-generated catch block
            log.error(e.getMessage(), e);
        }
        log.info("Saliendo");
    }

    private void cleanCounters() {
        long t = System.currentTimeMillis();
        for (String key : packetCounters.keySet()) {
            if (packetCounters.get(key).getTimestamp() < (t - TTL)) {
                packetCounters.remove(key);
            }
        }
    }

}
