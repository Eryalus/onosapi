package org.adrian.onosapi.processor.utils;

public class PacketCounter {

    private int count;
    private final long timestamp;

    public PacketCounter(long timestamp) {
        count = 0;
        this.timestamp = timestamp;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public int getCount() {
        return count;
    }

    public void addCount() {
        count++;
    }
}
