package org.adrian.onosapi.container;

import java.util.ArrayList;
import java.util.List;

import org.adrian.onosapi.entity.OvSwitch;

public class TopologyContainer {

    private List<OvSwitch> switches = new ArrayList<>();
    private static TopologyContainer container;

    public static TopologyContainer getInstance() {
        if (container == null)
            container = new TopologyContainer();
        return container;
    }

    private TopologyContainer() {
        container = this;
    }

    public List<OvSwitch> getSwitches() {
        return switches;
    }

    public void setSwitches(List<OvSwitch> switches) {
        this.switches = switches;
    }

}
