package org.adrian.onosapi.container;

import java.util.ArrayList;
import java.util.List;

import org.adrian.onosapi.entity.FwRuleGroup;

public class RuleContainer {

    private List<FwRuleGroup> rules = new ArrayList<>();
    private static RuleContainer container;

    public static RuleContainer getInstance() {
        if (container == null)
            container = new RuleContainer();
        return container;
    }

    private RuleContainer() {
        container = this;
    }

    public List<FwRuleGroup> getRules() {
        return rules;
    }

    public void setRules(List<FwRuleGroup> rules) {
        this.rules = rules;
    }

}
