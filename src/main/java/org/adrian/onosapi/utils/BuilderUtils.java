package org.adrian.onosapi.utils;

import java.util.Set;

import org.onosproject.net.flow.DefaultTrafficSelector;
import org.onosproject.net.flow.TrafficSelector.Builder;
import org.onosproject.net.flow.criteria.Criterion;

public class BuilderUtils {

    public static Builder cloneBuilderByCriteria(Builder builder){
        Builder cloned = DefaultTrafficSelector.builder();
        Set<Criterion> criteria = builder.build().criteria();
        for(Criterion crit : criteria){
            cloned.add(crit);
        }
        return cloned;
    }
}
