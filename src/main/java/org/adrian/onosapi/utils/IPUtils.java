package org.adrian.onosapi.utils;

import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.regex.Pattern;

import org.onlab.packet.IPv4;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IPUtils {

    private final Logger log = LoggerFactory.getLogger(getClass());
    private static final String IPv4_PATTERN_DEF = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
    private static final String IPv4_PATTERN = "^" + IPv4_PATTERN_DEF + "$";
    private static final String CIDR_PATTERN = "^" + IPv4_PATTERN_DEF + "[\\/](3[0-2]|[1-2][0-9]|[0-9])$";

    /**
     * Get InetAddress from packet.
     * 
     * @param packet
     * @return First element source, sencond destination. Null if error.
     */
    public static InetAddress[] getIPAddressesFromPacket(IPv4 packet) {
        InetAddress[] addresses = null;
        int sourceAddress = packet.getSourceAddress();
        int destinationAddress = packet.getDestinationAddress();
        byte[] srcIPb = BigInteger.valueOf(sourceAddress).toByteArray();
        byte[] dstIPb = BigInteger.valueOf(destinationAddress).toByteArray();
        try {
            InetAddress srcAddr = InetAddress.getByAddress(srcIPb);
            InetAddress dstAddr = InetAddress.getByAddress(dstIPb);
            addresses = new InetAddress[] { srcAddr, dstAddr };
        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
        }
        return addresses;
    }

    /**
     * Checks if the given IP addr is contained in the subnets
     * 
     * @param ip     IPv4
     * @param subnet IPv4 subnets CIDR separated by a comma (x.x.x.x/y,x.x.x.x/y,...)
     * @return
     */
    public static boolean isIPinSubnets(InetAddress ip, String subnets) throws UnknownHostException{
        boolean contains = false;
        for(String subnet : subnets.split(",")){
            contains = isIPinSubnet(ip, subnet);
            if(contains){
                break;
            }
        }
        return contains;
    }

    /**
     * Checks if the given IP addr is contained in the subnet
     * 
     * @param ip     IPv4
     * @param subnet IPv4 subnet CIDR
     * @return
     */
    public static boolean isIPinSubnet(InetAddress ip, String subnet) throws UnknownHostException {
        Pattern cidrPattern = Pattern.compile(CIDR_PATTERN);
        boolean contains = false;
        if (cidrPattern.matcher(subnet).find()) {
            String[] values = subnet.split("/"); // 0.0.0.0/0
            String cidrIP = values[0];
            int maskSize = Integer.parseInt(values[1]);
            if (maskSize == 0) {
                contains = true;
            } else {
                int ipInt = IPUtils.ipToInt(ip) >> (32 - maskSize);
                int subnetInt = IPUtils.ipToInt(Inet4Address.getByName(cidrIP)) >> (32 - maskSize);
                contains = ipInt == subnetInt;
            }
        }
        return contains;
    }

    private static int ipToInt(InetAddress inetAddress) {
        int result = 0;
        for (byte b : inetAddress.getAddress()) {
            result = result << 8 | (b & 0xFF);
        }
        return result;
    }
}
