package org.adrian.onosapi.utils;

import java.nio.ByteBuffer;

import org.onlab.packet.IPacket;
import org.onlab.packet.IPv4;
import org.onlab.packet.IPv6;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TCPUtils {

    public boolean IPPacketValid(IPacket packet) {
        boolean isTCP = false;
        if (packet instanceof IPv4) {
            isTCP = ((IPv4) packet).getProtocol() == IPv4.PROTOCOL_TCP;
        }
        return isTCP;
    }

    public L4Ports getPortsForPatcket(IPacket packet) {
        L4Ports ports = null;
        if (this.IPPacketValid(packet)) {
            IPacket innerPacket = packet.getPayload();
            byte[] packetBytes = innerPacket.serialize();
            if (packetBytes.length > 4) {
                ByteBuffer wrapped = ByteBuffer.wrap(new byte[] { 0, 0, packetBytes[0], packetBytes[1] });
                int source = wrapped.getInt();
                wrapped = ByteBuffer.wrap(new byte[] { 0, 0, packetBytes[2], packetBytes[3] });
                int dest = wrapped.getInt();
                ports = new L4Ports();
                ports.setSrcPort(source);
                ports.setDstPort(dest);
            }
        }
        return ports;
    }

}
