package org.adrian.onosapi.utils;

import org.onlab.packet.IPacket;
import org.onlab.packet.IPv4;

public class UDPUtils extends TCPUtils {

    @Override
    public boolean IPPacketValid(IPacket packet) {
        boolean isTCP = false;
        if (packet instanceof IPv4) {
            isTCP = ((IPv4) packet).getProtocol() == IPv4.PROTOCOL_UDP;
        }
        return isTCP;
    }
}
