package org.adrian.onosapi.utils;

import java.nio.ByteBuffer;

import org.onlab.packet.IPacket;
import org.onlab.packet.IPv4;

public class ICMPUtils {

    public boolean IPPacketValid(IPacket packet) {
        boolean isTCP = false;
        if (packet instanceof IPv4) {
            isTCP = ((IPv4) packet).getProtocol() == IPv4.PROTOCOL_ICMP;
        }
        return isTCP;
    }

    /**
     * Get packet type as source port.
     * 
     * @param packet Packet.
     * @return Type as source port or null.
     */
    public L4Ports getTypeAsPorts(IPacket packet) {
        L4Ports ports = null;
        if (this.IPPacketValid(packet)) {
            IPacket innerPacket = packet.getPayload();
            byte[] packetBytes = innerPacket.serialize();
            if (packetBytes.length > 1) {
                ByteBuffer wrapped = ByteBuffer.wrap(new byte[] { 0, 0, 0, packetBytes[0] });
                int source = wrapped.getInt();
                ports = new L4Ports();
                ports.setSrcPort(source);
                ports.setDstPort(0);
            }
        }
        return ports;
    }
}
