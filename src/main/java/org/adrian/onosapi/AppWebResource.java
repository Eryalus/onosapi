/*
 * Copyright 2021-present Open Networking Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.adrian.onosapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.adrian.onosapi.container.RuleContainer;
import org.adrian.onosapi.container.TopologyContainer;
import org.adrian.onosapi.entity.FwNetwork;
import org.adrian.onosapi.entity.FwRuleGroup;
import org.adrian.onosapi.entity.FwSubnet;
import org.adrian.onosapi.entity.OvSPort;
import org.adrian.onosapi.entity.OvSwitch;
import org.onlab.packet.IpAddress;
import org.onlab.rest.BaseResource;
import org.onosproject.net.Device;
import org.onosproject.net.DeviceId;
import org.onosproject.net.Host;
import org.onosproject.net.HostLocation;
import org.onosproject.net.Link;
import org.onosproject.net.device.DeviceService;
import org.onosproject.net.flow.FlowRuleService;
import org.onosproject.net.host.HostService;
import org.onosproject.net.link.LinkService;
import org.onosproject.net.topology.PathService;
import org.onosproject.net.topology.TopologyService;
import org.onosproject.rest.AbstractWebResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import static org.onlab.util.Tools.nullIsNotFound;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Sample web resource.
 */
@Path("v1")
public class AppWebResource extends AbstractWebResource {

    private final Logger log = LoggerFactory.getLogger(getClass());
    /**
     * Get hello world greeting.
     *
     * @return 200 OK
     */
    @GET
    @Path("/rules")
    public Response getRules() {
        ObjectNode node;
        try {
            node = mapper().createObjectNode().put("rules",
                    mapper().writeValueAsString(RuleContainer.getInstance().getRules()));
        } catch (JsonProcessingException e) {
            return Response.serverError().build();
        }
        return ok(node).build();
    }

    @GET
    @Path("/test")
    public Response test() {
        ObjectNode node;
        PathService pathService = get(PathService.class);
        HostService deviceService = get(HostService.class);
        Iterable<Host> devices = deviceService.getHosts();
        ArrayList<Host> list = new ArrayList<>();
        for (Host d : devices) {
            list.add(d);
        }
        // node = encodeArray(org.onosproject.net.Path.class, "paths", pathService
        //         .getPaths(DeviceId.deviceId("of:000000000000000b"), DeviceId.deviceId("of:000000000000000e")));
        node = encodeArray(Host.class, "hosts", list);
        return ok(node).build();
    }

    @POST
    @Path("/rules")
    public Response updateRules(String rules) {
        List<FwRuleGroup> groups = null;
        try {
            groups = mapper().readValue(rules, new TypeReference<List<FwRuleGroup>>() {
            });
            //remove all sdn rules
            FlowRuleService flowRuleService = get(FlowRuleService.class);
            flowRuleService.removeFlowRulesById(AppComponent.appId);
            //apply new rules
            RuleContainer.getInstance().setRules(groups);
        } catch (IOException e) {
        }
        if (groups == null) {
            return Response.status(400).build();
        }

        ObjectNode node;
        try {
            node = mapper().createObjectNode().put("hello", mapper().writeValueAsString(groups));
        } catch (JsonProcessingException e) {
            return Response.serverError().build();
        }
        return ok(node).build();
    }

    @GET
    @Path("/topology")
    public Response getTopology() {
        ObjectNode node;
        DeviceService deviceService = get(DeviceService.class);
        LinkService linkService = get(LinkService.class);
        HostService hostService = get(HostService.class);
        Iterable<Host> hosts = hostService.getHosts();
        Iterable<Device> availableDevices = deviceService.getAvailableDevices();
        HashMap<String, OvSwitch> switches = new HashMap<>();
        for(Device d : availableDevices){
            OvSwitch s = new OvSwitch();
            s.setOnosDeviceId(d.id().toString());
            s.setHumanName(d.chassisId().toString());
            switches.put(d.id().toString(), s);
        }
        Iterable<Link> links = linkService.getLinks();
        long portCounter = 0L;
        for(Link link: links){
            OvSPort port = new OvSPort();
            port.setId(portCounter++);
            port.setOnosPortName(link.src().port().toString());
            port.setHumanName(deviceService.getDevice(link.src().deviceId()).chassisId().toString()+"-"+link.src().port().toString());
            FwNetwork net = new FwNetwork();
            net.setHumanName("Default network "+port.getHumanName());
            port.addFwNetwork(net);
            switches.get(link.src().deviceId().toString()).addOvSPort(port);
        }
        for(Link link: links){
            Set<OvSPort> ovSPorts = switches.get(link.dst().deviceId().toString()).getOvSPorts();
            OvSPort destPort = null;
            for(OvSPort port : ovSPorts){
                if(port.getOnosPortName().equals(link.dst().port().toString())){
                    destPort = port;
                    break;
                }
            }
            if(destPort!=null){
                Set<OvSPort> ovSPorts2 = switches.get(link.src().deviceId().toString()).getOvSPorts();
                for(OvSPort port : ovSPorts2){
                    if(port.getOnosPortName().equals(link.src().port().toString())){
                        destPort.setOvSPort(port.getId());
                        port.setOvSPort(destPort.getId());
                    }
                }
            }
        }
        for(Host h : hosts){
            for(HostLocation location : h.locations()){
                Set<OvSPort> ovSPorts = switches.get(location.deviceId().toString()).getOvSPorts();
                OvSPort port = null;
                for(OvSPort port1: ovSPorts){
                    if(port1.getOnosPortName().equals(location.port().toString())){
                        port = port1;
                        break;
                    }
                }
                if(port == null){
                    port = new OvSPort();
                    port.setId(portCounter++);
                    port.setOnosPortName(location.port().toString());
                    port.setHumanName(deviceService.getDevice(location.deviceId()).chassisId().toString()+"-"+location.port().toString());
                    FwNetwork net = new FwNetwork();
                    net.setHumanName("Default network "+port.getHumanName());
                    port.addFwNetwork(net);
                    switches.get(location.deviceId().toString()).addOvSPort(port);
                }
                for(IpAddress addr : h.ipAddresses()){
                    FwSubnet subnet = new FwSubnet();
                    subnet.setAllow(true);
                    subnet.setPriority(0);
                    subnet.setIpPrefix(addr.toString());
                    subnet.setIpMask("32");
                    Set<FwNetwork> fwNetworks = port.getFwNetworks();
                    boolean added = false;
                    for(FwNetwork net : fwNetworks){
                        net.addFwSubnet(subnet);
                        added = true;
                        break;
                    }
                    if(!added){
                        FwNetwork net = new FwNetwork();
                        net.setHumanName("Default network "+port.getHumanName());
                        port.addFwNetwork(net);
                    }
                }
            }
        }
        try {
            node = mapper().createObjectNode().put("topology",
                    mapper().writeValueAsString(switches.values()));
        } catch (JsonProcessingException e) {
            return Response.serverError().build();
        }
        return ok(node).build();
    }

}
