package org.adrian.onosapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A OvSPort.
 */
public class OvSPort implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String humanName;

    private String onosPortName;

    private Set<FwNetwork> fwNetworks = new HashSet<>();

    @JsonIgnoreProperties(value = "ovSPorts", allowSetters = true)
    private OvSwitch ovSwitch;

    private Long ovSPort;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHumanName() {
        return humanName;
    }

    public OvSPort humanName(String humanName) {
        this.humanName = humanName;
        return this;
    }

    public void setHumanName(String humanName) {
        this.humanName = humanName;
    }

    public String getOnosPortName() {
        return onosPortName;
    }

    public OvSPort onosPortName(String onosPortName) {
        this.onosPortName = onosPortName;
        return this;
    }

    public void setOnosPortName(String onosPortName) {
        this.onosPortName = onosPortName;
    }

    public Set<FwNetwork> getFwNetworks() {
        return fwNetworks;
    }

    public OvSPort fwNetworks(Set<FwNetwork> fwNetworks) {
        this.fwNetworks = fwNetworks;
        return this;
    }

    public OvSPort addFwNetwork(FwNetwork fwNetwork) {
        this.fwNetworks.add(fwNetwork);
        fwNetwork.getOvSPorts().add(this);
        return this;
    }

    public OvSPort removeFwNetwork(FwNetwork fwNetwork) {
        this.fwNetworks.remove(fwNetwork);
        fwNetwork.getOvSPorts().remove(this);
        return this;
    }

    public void setFwNetworks(Set<FwNetwork> fwNetworks) {
        this.fwNetworks = fwNetworks;
    }

    public OvSwitch getOvSwitch() {
        return ovSwitch;
    }

    public OvSPort ovSwitch(OvSwitch ovSwitch) {
        this.ovSwitch = ovSwitch;
        return this;
    }

    public void setOvSwitch(OvSwitch ovSwitch) {
        this.ovSwitch = ovSwitch;
    }

    public Long getOvSPort() {
        return ovSPort;
    }

    public void setOvSPort(Long ovSPort) {
        this.ovSPort = ovSPort;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OvSPort)) {
            return false;
        }
        return id != null && id.equals(((OvSPort) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OvSPort{" +
            "id=" + getId() +
            ", humanName='" + getHumanName() + "'" +
            ", onosPortName='" + getOnosPortName() + "'" +
            "}";
    }
}
