package org.adrian.onosapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A FwRule.
 */
public class FwRule implements Serializable, Comparable<FwRule> {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String humanName;

    private Integer priority;

    private String action;

    private Set<FwFilter> fwFilters = new HashSet<>();

    @JsonIgnoreProperties(value = "fwRules", allowSetters = true)
    private FwRuleGroup fwRuleGroup;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHumanName() {
        return humanName;
    }

    public FwRule humanName(String humanName) {
        this.humanName = humanName;
        return this;
    }

    public void setHumanName(String humanName) {
        this.humanName = humanName;
    }

    public Integer getPriority() {
        return priority;
    }

    public FwRule priority(Integer priority) {
        this.priority = priority;
        return this;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getAction() {
        return action;
    }

    public FwRule action(String action) {
        this.action = action;
        return this;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Set<FwFilter> getFwFilters() {
        return fwFilters;
    }

    public FwRule fwFilters(Set<FwFilter> fwFilters) {
        this.fwFilters = fwFilters;
        return this;
    }

    public FwRule addFwFilter(FwFilter fwFilter) {
        this.fwFilters.add(fwFilter);
        fwFilter.setFwRule(this);
        return this;
    }

    public FwRule removeFwFilter(FwFilter fwFilter) {
        this.fwFilters.remove(fwFilter);
        fwFilter.setFwRule(null);
        return this;
    }

    public void setFwFilters(Set<FwFilter> fwFilters) {
        this.fwFilters = fwFilters;
    }

    public FwRuleGroup getFwRuleGroup() {
        return fwRuleGroup;
    }

    public FwRule fwRuleGroup(FwRuleGroup fwRuleGroup) {
        this.fwRuleGroup = fwRuleGroup;
        return this;
    }

    public void setFwRuleGroup(FwRuleGroup fwRuleGroup) {
        this.fwRuleGroup = fwRuleGroup;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FwRule)) {
            return false;
        }
        return id != null && id.equals(((FwRule) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FwRule{" +
            "id=" + getId() +
            ", humanName='" + getHumanName() + "'" +
            ", priority=" + getPriority() +
            ", action='" + getAction() + "'" +
            "}";
    }

    @Override
    public int compareTo(FwRule o) {
        return o.priority.compareTo(this.priority);
    }
}
